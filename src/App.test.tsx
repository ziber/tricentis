import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders base items', () => {
  render(<App />);
  const linkElement = screen.getByText(/C/i);
  expect(linkElement).toBeInTheDocument();
});
