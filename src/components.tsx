import styled from 'styled-components';

export const Flex = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`
export const Input = styled.input`
  border-radius: 5px;
  border: 1px solid gray;
  padding: 5px;
`

export const Layout = styled(Flex)`
  background: white;
  align-items: center;
  justify-content: center;
`

export const Page = styled(Flex)`
  width: 400px;
  margin: 50px 0;
`

export const Item = styled(Flex)`
  height: 30px;
  border: 1px solid black;
  margin: 5px 0;
  align-items: center;
  justify-content: center;
`

export const List = styled(Flex)`
  margin: 10px 0;
`
