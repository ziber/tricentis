
export const shiftArray = (arr: string[]) => arr.length > 1 ? [...arr.slice(1), arr[0]] : []

let timer: ReturnType<typeof setTimeout>

export function debounce(func: () => void, timeout = 500) {
  if(timer) {
    clearTimeout(timer);
  }
  timer = setTimeout(func, timeout);
}
