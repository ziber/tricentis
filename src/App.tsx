
import { useCallback } from 'react';
import { Flex , Input, Layout, Page, Item, List } from './components'
import { debounce } from './utils'
import { useItems, useApi } from './hooks'

function App() {
  const { items, updateItems } = useItems()
  const { fetchData, loading } = useApi(updateItems);
  const handleInputChange = useCallback((e) => debounce(() => fetchData(e.target.value)), [fetchData])

  return (
    <Layout>
      <Page>
        <Input
          placeholder="search github users by login"
          onChange={handleInputChange}
        />
        {loading && <Flex>Loading...</Flex>}
        <List>
          {items.map((item) => (
            <Item key={item}>{item}</Item>
          ))}
        </List>
      </Page>
    </Layout>
  );
}

export default App;
