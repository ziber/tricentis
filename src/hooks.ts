import { useState, useEffect, useCallback } from 'react';
import { shiftArray } from './utils'
import { SECOND, BASE_ITEMS, API_URL } from './constants'


let interval: ReturnType<typeof setInterval>

type DataItem = {
  login: string
}

type Response = {
  total_count: number
  items: DataItem[]
}

export function useItems() {
  const [items, setItems] = useState(BASE_ITEMS)

  const update = useCallback(() => setItems(shiftArray(items)), [items])

  useEffect(() => {
    interval = setInterval(update, SECOND);
    return () => {
      clearInterval(interval);
    }
  },[update])
  return {
    items: shiftArray(items).slice(0, Math.min(items.length, 5)),
    updateItems: (newItems: string[]) =>  setItems([...BASE_ITEMS, ...newItems])
  }
}

export type Items = ReturnType<typeof useItems>

export function useApi(updateItems: Items['updateItems']) {
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState('')

  return {
    loading,
    error,
    fetchData: async (term: string) => {
      if(!term?.length) {
        return false
      }
      setLoading(true);
      try {
        const response = await fetch(`${API_URL}?q=${encodeURIComponent(`${term} in:login`)}`)
        const data: Response = await response.json();
        const result = data?.items?.map(({ login }) => (login)) ?? []
        updateItems(result
          .sort((a, b) => a.localeCompare(b))
          .slice(0, Math.min(5, result.length))
        )
      } catch(e) {
        setError(String(e))
        console.error(e)
      }
      setLoading(false)
    }
  }
}
